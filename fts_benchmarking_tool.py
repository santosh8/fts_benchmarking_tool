import os
import json
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--max_vms', type=int, help='maximum number of virtual machines', required=True)
parser.add_argument('--load', type=int, help='load on the system', required=True)
parser.add_argument('--db', type=str, help='name of the database', required=True)
parser.add_argument('--endpoint', type=str, help='db endpoint', required=True)
parser.add_argument('--workload', type=str, help='workload a or b or c', required=True)
parser.add_argument('--pk', type=str, help='primary key for dynamodb', required=False)
parser.add_argument('--tablename', type=str, help='table name for dynamodb', required=False)
parser.add_argument('--region', type=str, help='region', required=False)

args = parser.parse_args()

max_vms = args.max_vms
load = args.load
db = args.db
endpoint = args.endpoint
workload = args.workload
pk = args.pk
tablename = args.tablename
region = args.region



def get_load_command(db, load, vm):
    ret =[]
    if db=="redis":
        for thread in [10, 50, 100]:
            threads=thread//vm
            ret.append("./bin/ycsb load redis -p recordcount={} -p redis.host='{}' -p redis.port=6379 -P workloads/workload{} -p redis.cluster=true -threads {} > /tmp/redis_load_out_{}_{}.txt".format(load//vm, endpoint, workload, threads, threads, vm))
    elif db=="memcached":
        for thread in [10, 50, 100]:
            threads=thread//vm
            ret.append("./bin/ycsb load memcached -p recordcount={} -P workloads/workload{} -p memcached.hosts={}:11211 -threads {} > /tmp/memcached_load_out_{}_{}.txt".format(load//vm, workload, endpoint, threads, threads, vm))
    elif db=="dynamodb":
        for thread in [10,50,100,500]:
            threads=thread//vm
            ret.append("./bin/ycsb load dynamodb -s -p recordcount={} -p dynamodb.endpoint='{}' -P workloads/workload{} -p dynamodb.primaryKey={} -p dynamodb.tablename={} -p dynamodb.region={} > /tmp/dynamodb_load_out_{}_{}.txt".format(load//vm, endpoint, workload, pk, tablename, region, threads, vm))

    return ret[0]

def get_run_command(db, load, vm):
    ret =[]
    if db=="redis":
        for thread in [10, 50, 100]:
            threads=thread//vm
            ret.append("./bin/ycsb run redis -p operationcount={} -p redis.host='{}' -p redis.port=6379 -P workloads/workload{} -p redis.cluster=true -threads {} > /tmp/redis_run_out_{}_{}.txt".format(load//vm, endpoint, workload, threads, threads, vm))
    elif db=="memcached":
        for thread in [10, 50, 100]:
            threads=thread//vm
            ret.append("./bin/ycsb run memcached -p operationcount={} -P workloads/workload{} -p memcached.hosts={}:11211 -threads {} > /tmp/memcached_run_out_{}_{}.txt".format(load//vm, workload, endpoint, threads, threads, vm))
    elif db=="dynamodb":
        for thread in [10,50,100,500]:
            threads=thread//vm
            ret.append("./bin/ycsb run dynamodb -p operationcount={} -p dynamodb.endpoint='{}' -P workloads/workload{} -p dynamodb.primaryKey={} -p dynamodb.tablename={} -p dynamodb.region={} > /tmp/dynamodb_run_out_{}_{}.txt".format(load//vm, endpoint, workload, pk, tablename, region, threads, vm))

    return ret


for vm in range(1,max_vms+1):
    run_commands = get_run_command(db, load, vm)
    os.system("sudo terraform apply -var='instance_count={}' -var='runcmd1={}' -var='runcmd2={}' -var='runcmd3={}' -var='loadcmd={}' -auto-approve".format(vm, run_commands[0], run_commands[1], run_commands[2], get_load_command(db, load, vm)))
    ips = []
    with open("terraform.tfstate") as tf:
        data=json.load(tf)
        ips = json.loads(data["outputs"]["ec2_public_ip"]["value"])
    for thread in [10, 50, 100]:
        threads=thread//vm
        for ip in ips:
            rep_ip = ip.replace(".","_")
            if thread==10//vm:
                os.system("sudo scp -o StrictHostKeyChecking=no -i cca.pem ec2-user@{}:/tmp/{}_load_out_{}_{}.txt ./{}_load_out_{}_{}_{}.txt".format(ip, db, threads, vm, db, rep_ip, threads, vm))
            os.system("sudo scp -o StrictHostKeyChecking=no -i cca.pem ec2-user@{}:/tmp/{}_run_out_{}_{}.txt ./{}_run_out_{}_{}_{}.txt".format(ip, db, threads, vm, db, rep_ip, threads, vm))
    os.system("sudo terraform destroy -var='instance_count={}' -var='runcmd1={}' -var='runcmd2={}' -var='runcmd3={}' -var='loadcmd={}' -auto-approve".format(vm, run_commands[0], run_commands[1], run_commands[2], get_load_command(db, load, vm)))

os.system("sudo python3 Graphs.py")
